import * as React from 'react';
import * as renderer from 'react-test-renderer';
import DataGridComponent from './index';

test('DataGrid should display 2 rows', () => {
  const data = [{ title: 'tomato' }, { title: 'potatoes' }];
  const config = [{ key: 'title' }];
  const dg = renderer.create(<DataGridComponent data={data} config={config} />)
  const tree = dg.toJSON();
  const table = tree.children[0];
  const rows = table.children[1].children;
  expect(rows.length).toBe(2);
})
