import * as React from 'react';

class DataGridComponent extends React.Component {

    props: any;

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            {this.props.config.map((head: any, idx: number) => {
                                return <th key={idx}>{head.key.toUpperCase()}</th>
                            })}
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.data.map((item: any, idx: number) => {
                            return <tr key={idx}>
                                {this.props.config.map((field: any, idx: number) => {
                                    switch (field.type) {
                                        case 'number':
                                            return <td key={item.id}>
                                                <input type="text"
                                                    onChange={(evt) => this.props.update(item.id, { [field.key]: +evt.target.value })}
                                                    className={'form-control'}
                                                    disabled={!this.props.access}
                                                    defaultValue={item[field.key]} />
                                            </td>
                                        case 'image':
                                            return <td key={idx}><img src={item[field.key]} width="50" /></td>
                                        default:
                                            return <td key={idx}>{item[field.key]}</td>
                                    }
                                })}
                                <td>
                                    <button className="btn btn-danger"
                                        disabled={!this.props.access}
                                        onClick={() => this.props.remove(item.id)}>
                                        remove
                                    </button>

                                </td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default DataGridComponent
