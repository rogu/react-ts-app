import * as React from 'react';

export interface Props {
    controls: any[];
    onChange: any;
  }

export class SearchComponent extends React.Component<Props> {

    constructor(props:any) {
        super(props);
        this.state = {};
    }

    searchBy({target: {dataset: {key}, value}}) {
        this.setState({[key]: value}, () => {
            this.props.onChange({...this.state});
        });
    }

    render() {
        return (
            <div className={'card card-body'}>
                {this.props.controls.map((control, idx) => {
                    return <label key={idx}>
                        search by {control}
                        <br/>
                        {<input type="text"
                                data-key={control}
                                onChange={this.searchBy.bind(this)}
                                className={'form-control'}/>}
                    </label>
                })}
            </div>
        );
    }
}