import { USER_LOGGED, USER_LOGGED_OUT } from './../../utils/constants';

export function authReducers(state = { access: false }, action) {
    switch (action.type) {
        case USER_LOGGED:
            return { ...state, access: true };
        case USER_LOGGED_OUT:
            return { ...state, access: false };
        default:
            return state;
    }
}