import * as React from "react"
import { authReducers } from './auth-reducers';
import connect from "react-redux/es/connect/connect";
import * as actions from './actions';
import ModalComponent from '../Modal/modal';

export interface Props {
    logIn: Function;
    logged: Function;
    logOut: any;
    access: boolean;
}

export interface State {
}

class Auth extends React.Component<Props, State> {

    authModal: any;
    constructor(props: Props) {
        super(props)

        this.state = {
        }
        this.authModal = React.createRef();
    }

    componentDidMount() {
        this.props.logged();
    }

    sendForm(evt) {
        evt.preventDefault();
        this.authModal.current.toggle();
        const { username: { value: username }, password: { value: password } } = evt.target;
        this.props.logIn({ username, password });
    }

    render() {
        return (
            <div>
                {this.props.access
                    ? <button className="btn btn-warning" onClick={this.props.logOut}>log out</button>
                    : <ModalComponent ref={this.authModal} buttonLabel={'log in'}>
                        <form className="form-inline" onSubmit={this.sendForm.bind(this)}>
                            <input className="form-control mr-2"
                                type="email"
                                name="username"
                                defaultValue={'admin@localhost'}
                                placeholder="username" />
                            <input className="form-control mr-2"
                                type="password"
                                name="password"
                                defaultValue={'admin'}
                                placeholder="password" />
                            <button className="btn btn-primary">
                                send
                            </button>
                        </form>
                    </ModalComponent>
                }
            </div>
        )
    }
}

export default connect(
    (state: any) => {
        return { ...state.authReducers }
    },
    (dispatch, props) => {
        return {
            logIn: (data) => {
                dispatch(actions.logIn(data))
            },
            logged() {
                dispatch(actions.logged())
            },
            logOut() {
                dispatch(actions.logout())
            }
        }
    }
)(Auth);