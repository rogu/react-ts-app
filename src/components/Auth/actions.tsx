import axios from 'axios';
import { Settings } from '../../utils/settings';
import { USER_LOGGED, USER_LOGGED_OUT } from '../../utils/constants';

export interface AuthDataInterface {
    username: string;
    password: string;
}

export function logIn(value: AuthDataInterface) {
    return function (dispatch) {
        axios.post(Settings.LOGIN_END_POINT, value)
            .then((resp) => {
                if (resp.data.ok) dispatch({ type: USER_LOGGED })
                else alert(JSON.stringify(resp.data))
            })
    }
}

export function logged() {
    return function (dispatch) {
        axios.get(Settings.LOGGED_END_POINT)
            .then((resp) => {
                if (resp.data.ok) dispatch({ type: USER_LOGGED })
            })
    }
}

export function logout() {
    return function (dispatch) {
        axios.get(Settings.LOGOUT_END_POINT)
            .then((resp) => {
                dispatch({ type: USER_LOGGED_OUT });
            })
    }
}