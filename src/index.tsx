import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { setCORSInterceptors } from './utils/cors-config';
import { itemsReducer } from './containers/Items/items-reducers';
import { authReducers } from './components/Auth/auth-reducers';
import { registerReducer } from './containers/Register/register-reducers';
import { workersReducers } from './containers/Workers/workers-reducers';

setCORSInterceptors();

const { Provider } = require('react-redux');

const reducers = combineReducers({
    itemsReducer,
    registerReducer,
    workersReducers,
    authReducers
});

const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}><App /></Provider>,
    document.getElementById('root') as HTMLElement
);
registerServiceWorker();

