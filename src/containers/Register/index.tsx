import * as React from 'react';

export class RegisterContainer extends React.Component {
    state: any;
    constructor(props: any) {
        super(props);
        this.state = {
            formValue: {
                username: '',
                password: ''
            }
        }
    }

    updateForm(evt: any) {
        const { value, validity, validationMessage, name, nextElementSibling } = evt.nativeEvent.target;
        nextElementSibling.classList[validity.valid ? 'add' : 'remove']('d-none');
        nextElementSibling.innerText = validationMessage;
        this.setState({ formValue: { ...this.state.formValue, [name]: value } });
    }

    render() {
        return <div>
            {JSON.stringify(this.state.formValue)}
            <form className={'jumbotron'}>
                <div className="form-group">
                    <label htmlFor="">
                        username
                        <input type="text"
                            required
                            pattern="^[\w\s]{3,}"
                            onChange={this.updateForm.bind(this)}
                            className="form-control"
                            name="username" />
                        <small className="badge badge-warning d-none">any error</small>
                    </label>
                </div>
                <div className="form-group">
                    <label htmlFor="">
                        password
                        <input type="text"
                            required
                            onChange={this.updateForm.bind(this)}
                            className="form-control"
                            name="password" />
                        <small className="badge badge-warning d-none">any error</small>
                    </label>
                </div>
                <button type={'button'} className="btn btn-primary">send</button>
            </form>
        </div>
    }
}
