import { FETCH_ITEMS_SUCCESS } from "../../utils/constants";

const init = {
    data: [],
    total: 0,
    config: [
        { key: 'title' },
        { key: 'price', type: 'number' },
        { key: 'imgSrc', type: 'image' },
    ]
}

export function itemsReducer(state = init, action: any) {
    switch (action.type) {
        case FETCH_ITEMS_SUCCESS:
            return { ...state, data: action.payload.data, total: action.payload.total };
        default:
            return state;
    }
}
