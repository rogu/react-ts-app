import * as React from 'react';
import DataGridComponent from '../../components/DataGrid';
import { SearchComponent } from '../../components/Search';
import { connect } from 'react-redux';
import { fetchItems, addItem, removeItem, updateItem } from './actions';
import { ThunkAction } from 'redux-thunk';
import CheckboxWithLabel from '../../components/CheckBox/CheckboxWithLabel';
import AddItemFormComponent from './components/add-item-form';
import Pagination from "react-js-pagination";
import ModalComponent from '../../components/Modal/modal';
import { debounce } from '../../utils/helpers';

class ItemsComponent extends React.Component<ItemsProps> {

    filters: any;
    addItemModal: any;

    constructor(props) {
        super(props);
        this.filters = {
            itemsPerPage: 5,
            currentPage: 1
        }
        this.addItemModal = React.createRef();
    }

    componentDidMount() {
        this.props.fetchItems(this.filters);
    }

    search(filters) {
        this.filters = { ...this.filters, ...filters, currentPage: 1 }
        this.props.fetchItems(this.filters);
    }

    addItem(evt) {
        evt.preventDefault();
        if (evt.target.checkValidity()) {
            this.addItemModal.current.toggle();
            const { title: { value: title }, price: { value: price }, category: { value: category } } = evt.target;
            this.props.addItem({ title, price, category }, this.filters);
        }
    }

    updateFilters(key, value) {
        this.filters = { ...this.filters, [key]: value };
        this.props.fetchItems(this.filters);
    }

    render() {
        return <div className="row">
            <div className="col-3">
                {/* <CheckboxWithLabel labelOn="on" labelOff="off" /> */}
                <SearchComponent onChange={this.search.bind(this)} controls={['title', 'priceFrom']} />
            </div>
            <div className="col-9">
                <div className="row">
                    <div className="col-2">
                        <ModalComponent ref={this.addItemModal} buttonLabel={'add item'}>
                            <AddItemFormComponent sendForm={this.addItem.bind(this)} />
                        </ModalComponent>
                    </div>
                    <div className="col-2">
                        <select
                            defaultValue={this.filters.itemsPerPage}
                            className="form-control"
                            onChange={(evt) => {
                                this.updateFilters('itemsPerPage', evt.target.value);
                            }}>
                            {[2, 5, 10].map((opt, idx) => <option key={idx}>{opt}</option>)}
                        </select>
                    </div>
                    <div className="col-8">
                        <Pagination
                            activePage={this.filters.currentPage}
                            itemsCountPerPage={this.filters.itemsPerPage}
                            totalItemsCount={this.props.total}
                            itemClass={'page-item'}
                            linkClass={'page-link'}
                            activeLinkClass={'active'}
                            pageRangeDisplayed={5}
                            onChange={this.updateFilters.bind(this, 'currentPage')}
                        />
                    </div>
                </div>
                <DataGridComponent data={this.props.data}
                    remove={(id) => {
                        this.props.remove(id, this.filters);
                    }}
                    update={this.props.update}
                    access={this.props.access}
                    config={this.props.config} />
            </div>
        </div>
    }
}

export default connect(
    (state: any) => {
        return { ...state.itemsReducer, ...state.authReducers }
    },
    (dispatch) => {
        return {
            fetchItems(filters) {
                dispatch(fetchItems(filters));
            },
            search(filters) {
                dispatch(fetchItems(filters));
            },
            addItem(item, filters) {
                dispatch(addItem(item, filters))
            },
            remove(id, filters) {
                dispatch(removeItem(id, filters));
            },
            update(id, data) {
                debounce(() => dispatch(updateItem(id, data)))
            }
        }
    }
)(ItemsComponent);
