import * as React from "react"

export interface Props {
    data: any,
    match: { params }
}

export interface State {
}

export default class ItemDetails extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <div>
                item id:
                {JSON.stringify(this.props.match.params.id)}
            </div>
        )
    }
}
