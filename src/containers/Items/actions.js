import axios from 'axios';
import { FETCH_ITEMS_SUCCESS } from "../../utils/constants";
import queryString from 'query-string';
import { Settings } from '../../utils/settings';

export function addItem(item, filters) {
    return (dispatch) => {
        axios.post(Settings.ITEMS_END_POINT, item)
            .then((resp) => {
                return dispatch(fetchItems(filters));
            })
    }
}

export function fetchItems(filters) {
    return (dispatch) => {
        axios
            .get(`${Settings.ITEMS_END_POINT}?${queryString.stringify(filters)}`)
            .then((resp) => {
                return dispatch({ type: FETCH_ITEMS_SUCCESS, payload: resp.data });
            });
    }
}

export function removeItem(id, filters) {
    return (dispatch) => {
        axios
            .delete(`${Settings.ITEMS_END_POINT}/${id}`)
            .then((resp) => {
                return dispatch(fetchItems(filters));
            })
    }
}

export function updateItem(id, data) {
    return (dispatch) => axios.patch(`${Settings.ITEMS_END_POINT}/${id}`, data);
}

