interface ItemsProps {
    search: Function;
    addItem: Function;
    fetchItems: Function;
    remove: Function;
    update: Function;
    data: any[];
    total: number;
    config: any[];
    access: boolean;
}