import { FETCH_WORKERS_SUCCESS, SEARCH_WORKERS } from '../../utils/constants';
const init = {
    data: [],
    config: [
        { key: 'name' },
        { key: 'phone', type: 'number' }
    ]
}
let dataCopy;
export function workersReducers(state = init, action) {

    switch (action.type) {
        case FETCH_WORKERS_SUCCESS:
            dataCopy = action.payload;
            return { ...state, data: dataCopy }
        case SEARCH_WORKERS:
            return {
                ...state,
                data: dataCopy.filter((item) => {
                    for (let key in action.payload) {
                        if (!item[key].toLowerCase().includes(action.payload[key].toLowerCase())) return;
                    }
                    return true;
                })
            }
        default:
            return state;
    }

}