import * as React from 'react';
import { connect } from 'react-redux';
import { SearchComponent } from '../../components/Search/index';
import DataGridComponent from '../../components/DataGrid';
import { workersReducers } from './workers-reducers';
import * as actions from './actions';
import { SEARCH_WORKERS } from '../../utils/constants';
import ModalComponent from '../../components/Modal/modal';

class WorkersComponent extends React.Component<WorkersProps> {

    modal;

    constructor(props) {
        super(props);
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.fetchWorkers();
    }

    sendForm(evt) {
        evt.preventDefault();
        this.modal.current.toggle();
    }

    render() {
        return (
            <div className="row">
                <div className="col-3">
                    <SearchComponent controls={['name', 'phone']} onChange={this.props.search} />
                </div>
                <div className="col-9">
                    <ModalComponent ref={this.modal} buttonLabel={'add item'}>
                        <button onClick={this.sendForm.bind(this)}>close me. not implemented yet!</button>
                    </ModalComponent>
                    <DataGridComponent data={this.props.data} 
                                        access={this.props.access} 
                                        remove={()=>{alert('not implemented yet!')}}
                                        config={this.props.config} />
                </div>
            </div>
        );
    }
}

export default connect(
    (state: any) => {
        return { ...state.workersReducers, ...state.authReducers }
    },
    (dispatch) => {
        return {
            fetchWorkers: () => dispatch(actions.fetchWorkers()),
            search: (text) => dispatch({ type: SEARCH_WORKERS, payload: text })
        }
    }
)(WorkersComponent);