interface WorkersProps {
    search: Function;
    fetchWorkers: Function;
    data: any[];
    config: any[];
    access: boolean;
}