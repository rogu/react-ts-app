import axios from 'axios';
import { Settings } from '../../utils/settings';
import { FETCH_WORKERS_SUCCESS } from '../../utils/constants';
export function fetchWorkers() {
    return function (dispatch) {
        axios.get(Settings.WORKERS_END_POINT)
            .then((resp) => {
                dispatch({ type: FETCH_WORKERS_SUCCESS, payload: resp.data.data });
            })
    }
}