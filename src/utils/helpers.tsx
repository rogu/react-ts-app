export const debounce = (() => {
    let delay;
    return (cb, time = 1000) => {
        clearTimeout(delay);
        delay = setTimeout(() => cb(), time);
    }
})();