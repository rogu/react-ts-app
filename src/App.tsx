import * as React from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import { RegisterContainer } from './containers/Register';
import ItemsComponent from './containers/Items';
import WorkersComponent from './containers/Workers';
import Auth from './components/Auth/Auth';
import ItemDetails from './containers/Items/components/ItemDetails/ItemDetails';

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <nav className="navbar navbar-expand bg-danger">
              <div className="nav-item">
                <NavLink className={'nav-link'} activeClassName={'nav-active'}
                  to={'/items'}>items</NavLink>
              </div>
              <div className="nav-item">
                <NavLink className={'nav-link'} activeClassName={'nav-active'}
                  to={'/register'}>register</NavLink>
              </div>
              <div className="nav-item">
                <NavLink className={'nav-link'} activeClassName={'nav-active'}
                  to={'/workers'}>workers</NavLink>
              </div>
              <div>
                  <Auth />
              </div>
            </nav>
            <div className="container-fluid mt-3">
              <Route exact path='/items' component={ItemsComponent}></Route>
              <Route path='/items/:id' component={ItemDetails}></Route>
              <Route path='/workers' component={WorkersComponent}></Route>
              <Route path='/register' component={RegisterContainer}></Route>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
